/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package airline;

import java.util.Date;

/**
 *
 * @author mymac
 */
public class Flight {
   
    private Date date;
    private String number;
    
    Flight(Date date, String number){
        this.date = date;
        this.number= number;
    }
    
    public String getNumber() {
        return number;
    }
    
    public Date getDate() {
        return date;
    }

}

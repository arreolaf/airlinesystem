/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package airline;

/**
 *
 * @author mymac
 */
public class Airplane {
    
    private String make;
    private String model;
    private String iD;
    private String numberOfSeats;
    
    Airplane(String make, String model, String iD, String numberOfSeats){
        this.make = make;
        this.model= model;
        this.iD = iD;
        this.numberOfSeats = numberOfSeats;
    }
    
    public String getMake() {
        return make;
    }
    public String getModel() {
        return model;
    }
    public String getID() {
        return iD;
    }
    public String getNumberOfSeats() {
        return numberOfSeats;
    }

    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package airline;

import java.util.Date;

/**
 *
 * @author mymac
 */
public class TestFlight {

    public static void main(String[] args) {
        
        Date date =  new Date(0);
        
        Airport myAirport = new Airport("YYZ","Toronto");
        Airline myAirline = new Airline("Air Canada", "ACC");
        Flight myFlight = new Flight(date, "12345");
        Airplane myAirplane = new Airplane("Boeing","737 MAX","B-78","336");
        
        String output = myAirline.getName() + "operates flight:" + myFlight.getNumber()+
        "that is schedule for: "+ myFlight.getDate();
        
        String output2 = "Airplane with make "+ myAirplane.getMake() +
                "and model "+ myAirplane.getModel() + "is set for flight on "+
                myFlight.getDate();

        
    }
    
}

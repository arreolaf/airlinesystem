/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package airline;

/**
 *
 * @author mymac
 */
public class Airport {
    
    private String airportCode;
    private String city;
    
    Airport(String airportCode, String city){
        this.airportCode= airportCode;
        this.city = city;
    }
    
    public String getAirportCode() {
        return airportCode;
    }
    
    public String getCity() {
        return city;
    }

    
}
